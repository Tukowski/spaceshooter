﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public GameObject Ship;
	public GameObject AsteroidPrefab;

	public float spawnDistance;			//pojawianie się od statku   10
	public float spawnInceremnt; 			//ile musi się przemieścić   2
	public float spawnPointer;			//odległość Spawnu od statku 5


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Ship.transform.position.z >= spawnDistance){
			GameObject AsteroidObject = Instantiate(AsteroidPrefab);

			AsteroidObject.transform.position = new Vector3 (
				Random.Range(-4,7),		//x
				Random.Range(-4,8),		//y
				Ship.transform.position.z + spawnPointer
			);

			AsteroidObject.GetComponent<Rigidbody>().velocity = new Vector3(Random.Range(0,10),Random.Range(0,10),Random.Range(0,10));
			spawnDistance = Ship.transform.position.z + spawnInceremnt;
		}
	}
}
