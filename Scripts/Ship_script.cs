﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ship_script : MonoBehaviour {

	public Transform MainCamera;

	public float shipSpeed = 2.0f;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		this.transform.rotation = MainCamera.rotation;
		transform.position += transform.forward * shipSpeed * Time.deltaTime;
		this.transform.rotation = new Quaternion (0, 0, 0, 0);
	}

	void OnTriggerEnter(Collider collider){
		if(collider.tag == "asteroid"){
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}
}