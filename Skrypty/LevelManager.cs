﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public void wczytajPoziom(int jakiPoziom_INT){
		Debug.Log("## Wybrano poziom: "+jakiPoziom_INT);
		SceneManager.LoadScene(jakiPoziom_INT);
	}
	public void zamknijGre(){
		Debug.Log("################# Zamykanie GRY #################");
		Application.Quit();
	}
}
