﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NiszczycielPociskowPozaMapa : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "Pocisk" || collider.gameObject.tag == "PociskPrzeciwnika") {
			Destroy(collider.gameObject);
		}
	}
}
