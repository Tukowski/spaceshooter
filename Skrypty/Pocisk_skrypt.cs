﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pocisk_skrypt : MonoBehaviour {

	public float obrazeniaPocisku = 1f;

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "Przeciwnik"){
			Destroy(this.gameObject);
		}
	}

}
