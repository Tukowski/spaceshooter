﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPrzeciwnikow : MonoBehaviour {

	public GameObject przeciwnikJaki;
	public float szerokosc=14f;
	public float wysokosc = 10f;
	public float szybkoscRuchuFormacji=5f;
	public float ograniczenieXzPrawej = 9.5f;
	public float ograniczenieXzLewej = 6.5f;

	private bool czyPrawo=true;


	// Use this for initialization
	void Start () {
		//@@@@@@@@ dla każdego obiektu przyporządkowuje pozycje
		foreach(Transform child in transform){
			GameObject przeciwnik = Instantiate(przeciwnikJaki, child.transform.position, Quaternion.identity) as GameObject;
			przeciwnik.transform.parent = child;
		}
	}

	//@@@@@@@rysowanie obszaru formacji.
	public void OnDrawGizmos(){
		Gizmos.DrawWireCube(this.transform.position, new Vector3 (szerokosc, wysokosc));
	}
	
	// Update is called once per frame
	void Update () {

		//@@@@@ poruszanie formacja
		if(czyPrawo){
			transform.position += new Vector3 (szybkoscRuchuFormacji * Time.deltaTime, 0);
			if (this.transform.position.x >= ograniczenieXzPrawej)
				czyPrawo = false;
		}else{
			transform.position += new Vector3 (-szybkoscRuchuFormacji * Time.deltaTime, 0);
			if(this.transform.position.x <= ograniczenieXzLewej)
				czyPrawo = true; 
		}

	}
}
