﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatekGracza : MonoBehaviour {

	public float predkoscGracza= 3;
	public GameObject pocisk;
	public PociskPrzeciwnika_skrypt pociskPrzeciwnika;
	public float szybkoscPocisku = 5.0f;
	public float zycieGracza = 10f;

	// Update is called once per frame
	void Update () {

		//@@@@@@@@@@ poruszanie statkiem
		if(Input.GetKey(KeyCode.LeftArrow)){
			transform.position += new Vector3(-predkoscGracza*Time.deltaTime,0,0);
		}if(Input.GetKey(KeyCode.RightArrow)){
			transform.position += new Vector3(predkoscGracza*Time.deltaTime,0,0);
		}if(Input.GetKey(KeyCode.UpArrow)){
			transform.position += new Vector3(0,predkoscGracza*Time.deltaTime,0);
		}if(Input.GetKey(KeyCode.DownArrow)){
			transform.position += new Vector3(0,-predkoscGracza*Time.deltaTime,0);
		}
		//@@@@@@@@@
		if(Input.GetKeyDown(KeyCode.Space)){
			GameObject Pocisk = Instantiate(pocisk, this.transform.position, Quaternion.identity) as GameObject;
			Pocisk.GetComponent<Rigidbody2D>().velocity = new Vector3 (0, szybkoscPocisku, 0);
		}

		//@@@@@@@@@@@ ograniczenie ruchu gracza
		float nowyX = Mathf.Clamp(transform.position.x, 0.35f, 15.65f);
		float nowyY = Mathf.Clamp(transform.position.y, 0.35f, 11.65f);

		transform.position = new Vector3 (nowyX, nowyY, transform.position.z);	
	}

	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "PociskPrzeciwnika"){
			print("Trafienie@@@@@@@@");
			zycieGracza -= pociskPrzeciwnika.obrazeniaPocisku;
			if (zycieGracza <= 0)
				Destroy(this.gameObject);
		}
	}
}
