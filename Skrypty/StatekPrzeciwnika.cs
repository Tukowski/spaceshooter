﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatekPrzeciwnika : MonoBehaviour {

	public Pocisk_skrypt pocisk;
	public float zyciePrzeciwnika = 2f;
	public GameObject pociskGameObject;
	public float szybkoscPocisku = -5.0f;


	void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.tag == "Pocisk"){
			print("Trafienie@@@@@@@@");
			zyciePrzeciwnika -= pocisk.obrazeniaPocisku;
			if (zyciePrzeciwnika <= 0)
				Destroy(this.gameObject);
		}
	}

	void Update(){
		GameObject Pocisk = Instantiate(pociskGameObject, this.transform.position, Quaternion.identity) as GameObject;
		Pocisk.GetComponent<Rigidbody2D>().velocity = new Vector3 (0, szybkoscPocisku, 0);
	
	}
}